/* $Id$ */
$(document).ready(function(){
    $('#edit-value').css('background', '#'+Drupal.settings.config.defaultColor);

    $('#edit-value').ColorPicker({
        color: '#'+Drupal.settings.config.defaultColor,
        onSubmit: function(hsb, hex, rgb, el) {
        $(el).val(hex);
            $(el).css('background', '#' + hex);
		    $(el).ColorPickerHide();
	    }
    });

    $('.countrycolor').each(function(i, element){
        $(element).css('background', '#' + $(element).val());
        $(element).ColorPicker({
            color: '#' + $(element).val(),
            onSubmit: function(hsb, hex, rgb, el) {
		        $(el).val(hex);
                $(el).css('background', '#' + hex);
		        $(el).ColorPickerHide();
	        }
        });
    });
});