<?php
// $Id$
/**
 * @file
 * World boundaries. Settings Page
 */


//return form

function world_boundaries_form() {
    drupal_add_js(drupal_get_path('module', 'world_boundaries') .'/world_boundaries_admin.js');
    drupal_add_js(array('config' => array('defaultColor'=> variable_get('world_boundaries_default_color','42A1D5')                                          )
                       ),
                       'setting'
                 );
    return  theme('world_boundaries_menu').
            drupal_get_form('world_boundaries_settings_def_color_form').
            drupal_get_form('world_boundaries_settings_form').
            drupal_get_form('world_boundaries_show_form');
}

//Settings form
function world_boundaries_settings_def_color_form() {
    $form['default_color'] = array(
        '#type' => 'fieldset',
        '#title' => t('Settings')
    );

    $form['default_color']['key'] = array(
        '#type' => 'textfield',
        '#default_value' => variable_get('world_boundaries_key',''),
        '#name' => 'world_boundaries_key',
        '#title' => t('Key')
    );

    $form['default_color']['width_map'] = array(
        '#type' => 'textfield',
        '#default_value' => variable_get('world_boundaries_width_map', 500),
        '#name' => 'world_boundaries_width_map',
        '#title' => t('Width of map'),
        '#size' => 4,
        '#maxlength' => 4,
        '#suffix' => 'px'
    );


    $form['default_color']['height_map'] = array(
        '#type' => 'textfield',
        '#default_value' => variable_get('world_boundaries_height_map',300),
        '#name' => 'world_boundaries_height_map',
        '#title' => t('Height of map'),
        '#size' => 4,
        '#maxlength' => 4,
        '#suffix' => 'px'
    );


    $form['default_color']['zoom'] = array(
        '#type' => 'textfield',
        '#default_value' => variable_get('world_boundaries_zoom','3'),
        '#size' => 2,
        '#maxlength' => 2,
        '#name' => 'world_boundaries_zoom',
        '#title' => t('Zoom')
    );

    $form['default_color']['center_y'] = array(
        '#type' => 'textfield',
        '#default_value' => variable_get('world_boundaries_center_y','48.0'),
        '#name' => 'world_boundaries_center_y',
        '#title' => t('Coordinate of the center Y')
    );

    $form['default_color']['center_x'] = array(
        '#type' => 'textfield',
        '#default_value' => variable_get('world_boundaries_center_x','38.0'),
        '#name' => 'world_boundaries_center_x',
        '#title' => t('Coordinate of the center X')
    );

    $form['default_color']['opacity'] = array(
        '#type' => 'textfield',
        '#default_value' => variable_get('world_boundaries_opacity','0.7'),
        '#size' => 6,
        '#maxlength' => 6,
        '#name' => 'world_boundaries_opacity',
        '#title' => t('Opacity')
    );

    $form['default_color']['value'] = array(
        '#type' => 'textfield',
        '#default_value' => variable_get('world_boundaries_default_color','42A1D5'),
        '#size' => 6,
        '#maxlength' => 6,
        '#name' => 'color_value',
        '#title' => t('Default color')
    );

    $form['default_color']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Change Settings'),
        '#name' => 'change_def_color'

    );

    return $form;
}

function world_boundaries_settings_def_color_form_submit($form, &$form_state) {
    if($form['#post']['color_value'] != '' && strlen($form['#post']['color_value']) == 6) {
        variable_set('world_boundaries_default_color',$form['#post']['color_value']);
    }

    if($form['#post']['world_boundaries_key'] != '') {
        variable_set('world_boundaries_key',$form['#post']['world_boundaries_key']);
    }

    if($form['#post']['world_boundaries_zoom'] != '') {
        variable_set('world_boundaries_zoom',$form['#post']['world_boundaries_zoom']);
    }

    if($form['#post']['world_boundaries_center_y'] != '') {
        variable_set('world_boundaries_center_y',$form['#post']['world_boundaries_center_y']);
    }

    if($form['#post']['world_boundaries_center_x'] != '') {
        variable_set('world_boundaries_center_x',$form['#post']['world_boundaries_center_x']);
    }

    if($form['#post']['world_boundaries_opacity'] != '') {
        variable_set('world_boundaries_opacity',$form['#post']['world_boundaries_opacity']);
    }

    if($form['#post']['world_boundaries_width_map'] != '') {
        variable_set('world_boundaries_width_map',$form['#post']['world_boundaries_width_map']);
    }

    if($form['#post']['world_boundaries_height_map'] != '') {
        variable_set('world_boundaries_height_map',$form['#post']['world_boundaries_height_map']);
    }

    drupal_set_message(t('Data has been save.'));
}

//Add city form
function world_boundaries_settings_form() {
    $dir = opendir(drupal_get_path('module', 'world_boundaries').'/WorldCountriesAsPolylines/');

    while($filename = readdir($dir)) {
        if(preg_match('/^([A-z])/', $filename)) {
            $optionsTemp[] = $filename;
        }
    }
    closedir($dir);
    asort($optionsTemp);

    foreach($optionsTemp as $value) {
        $options[$value] = t($value);
    }

    $form['addCountry'] = array(
        '#type' => 'fieldset',
        '#title' => t('Add Country')
    );

    $form['addCountry']['countriesName'] = array(
        '#type' => 'select',
        '#options' => $options
    );
    $form['addCountry']['addSubmit'] = array(
      '#type' => 'submit',
      '#value' => t('Add Country')
    );
    return $form;
}

function world_boundaries_settings_form_submit($form, &$form_state) {
    $data = db_query('SELECT COUNT(id) as count FROM {world_boundaries} WHERE country_name="%s"', $form['#post']['countriesName']);
    $count = db_fetch_object($data);
    if($count->count != 1) {
        if(db_query('INSERT INTO {world_boundaries} (country_name) VALUES ("%s")', $form['#post']['countriesName'])) {
            drupal_set_message(t('The country has been added'));
        } else {
            drupal_set_message(t("The country hasn't been added, try again"));
        }
    } else {
        drupal_set_message(t("This country already added"));
    }

}

function world_boundaries_show_form() {
    $result = db_query('SELECT COUNT(id) as count FROM {world_boundaries}');
    $count = db_fetch_object($result);
    if($count->count > 0) {
        $result = db_query('SELECT * FROM {world_boundaries}');
    }

    $form['countriesList'] = array(
        '#type' => 'fieldset',
        '#title' => t('Countries')
    );

    while($obj = db_fetch_object($result)) {

        if($obj->color === null) {
            $color = variable_get('world_boundaries_default_color','42A1D5');
        } else {
            $color = $obj->color;
        }

        $form['countriesList']['country['.$obj->id.']'] = array(
              '#type' => 'textfield',
              '#default_value' => $color,
              '#size' => 6,
              '#title' => $obj->country_name,
              '#attributes' => array('class' => 'countrycolor')
        );

        $form['countriesList']['delcountry['.$obj->id.']'] = array(
              '#type' => 'checkbox',
              '#title' => t('delete')
        );
    }
    if($count->count > 0) {
        $form['countriesList']['submit'] = array(
                '#type' => 'submit',
                '#value' => t('Save'),
        );
    }
    return $form;
}

function world_boundaries_show_form_submit($form, &$form_state) {
    foreach($form['#post']['country'] as $country_id => $country_color) {
        if(isset($form['#post']['delcountry'][$country_id])) {
            db_query('DELETE FROM {world_boundaries} WHERE id = %d', $country_id);
        } else {
            db_query("UPDATE {world_boundaries} SET color = '%s' WHERE id = %d", $country_color, $country_id);
        }
    }
    drupal_set_message(t('Data has been save.'));
}