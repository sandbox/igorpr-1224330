<?php
// $Id$
/**
 * @file
 *
 * Display googlemap
 */
?>
<div id="map" style="width: <?php print $m_width; ?>px; height: <?php print $m_height; ?>px"></div>
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=false&amp;key=<?php print variable_get('world_boundaries_key',''); ?>&amp;hl=<?php print $language->language; ?>" type="text/javascript"></script>