/* $Id$ */
/**
 * @file
 * Gmail api and paint function
 *
 */

var map;
window.onload = load;
//window.onunload = GUnload;

function load() {

    if(typeof(G_API_VERSION)=='undefined') return;
	if (GBrowserIsCompatible()) {

	    map = new GMap2(document.getElementById("map"));
        map.addControl(new GSmallZoomControl());
		map.setCenter(new GLatLng(Drupal.settings.config.center_y * 1, Drupal.settings.config.center_x * 1), Drupal.settings.config.zoom * 1);

        //Drupal.settings.config.func

        lineCalling();

		map.checkResize();
	} else {
		alert("Sorry, the Google Maps API is not compatible with this browser");
	}
}

function loadCountry(url, countryColor) {
    if(countryColor == '#') {
        countryColor =  Drupal.settings.config.defaultColor;
    }
	GDownloadUrl(url,
		function(data, responseCode){
			if((200==responseCode)&&(0!=data.length)) {
				try {
					var iStart = 0;
					var arePoints = false;
					var polylineLevels = "";
					//File structure is [encoded Levels<br>encoded Polylines<br>...]
					do {
						var iEnd = data.indexOf("<br>",iStart);
						if( false == arePoints) {
						    //Levels
							polylineLevels = data.substring(iStart,iEnd);
							arePoints = true;
						} else {
						    //Points

							var polygon = new  GPolygon.fromEncoded({
							    polylines: [{color: countryColor, weight: 2, opacity:1.0,
							    points: data.substring(iStart,iEnd),
							    levels: polylineLevels,
							    zoomFactor: 2, numLevels: 18}],
							    fill: true, color: countryColor, opacity: Drupal.settings.config.opacity, outline: false
                            });
							map.addOverlay(polygon);
							arePoints = false;
                        }
						iStart = iEnd + 4;//jump above '<br>' to the next polylines
					}
                    while( data.length > iStart)
				}
                catch(e){
						//here code to deal with errors;
				}
			}
			else {
					//here code to deal with errors;
			}
		});
}